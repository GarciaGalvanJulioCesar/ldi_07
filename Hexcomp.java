
public class Hexcomp {

    
    public static String toHexString(int decimal)
{
    String codes = "0123456789ABCDEF";

    StringBuilder builder = new StringBuilder(8);

    builder.setLength(8);

    for (int i = 7; i >= 0; i--) {
        builder.setCharAt(i, codes.charAt(decimal & 0xF));
        decimal >>= 4;
    }

    return builder.toString();
}
    
}

